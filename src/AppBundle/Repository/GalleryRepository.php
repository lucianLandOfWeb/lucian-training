<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class GalleryRepository extends EntityRepository
{
    /**
     * @param int $currentPage
     * @return mixed
     */
    public function getMostRecentGalleries($currentPage = 1, $limit)
    {
        $query = $this->createQueryBuilder('g')
                ->addOrderBy('g.createdAt', 'DESC')
                ->getQuery();

        return $this->paginate($query, $currentPage, $limit);
    }

    public function getMostPopularGalleries()
    {
        $query = $this->createQueryBuilder('g')
            ->addOrderBy('g.favouriteCounter', 'DESC')
            ->getQuery();

        return $query->getResult();
    }

    public function paginate($dql, $page = 1, $limit = 5)
    {
        $paginator = new Paginator($dql);
        $paginator->setUseOutputWalkers(false);

        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit);

        return $paginator;
    }

    public function getAllGalleries($currentPage = 1, $limit)
    {
        $query = $this->createQueryBuilder('g')
            ->getQuery()
            ->getScalarResult();

        return array_slice($query, ($currentPage - 1) * $limit, $limit);
    }

    public function getSearchedGalleries($searchValue)
    {
        $query = $this->createQueryBuilder('g')
            ->where('g.name like :galleryName')
            ->setParameter('galleryName', '%'.$searchValue.'%')
            ->getQuery();

        return $query->getScalarResult();
    }
}
