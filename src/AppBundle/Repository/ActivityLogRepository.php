<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ActivityLogRepository extends EntityRepository
{
    public function getUsersWithMostLikes($date)
    {
        $query = $this->createQueryBuilder('a');

        $query->select('a.userId, a.numberOfLikes')
                ->where('a.date = :date')
                ->orderBy('a.numberOfLikes', 'DESC')
                ->setMaxResults(3)
                ->setParameter(':date', $date);

        $qb = $query->getQuery();

        return $qb->getResult();
    }

    public function getMostActiveUsers($date)
    {
        $query = $this->createQueryBuilder('a');

        $query->select('a.userId, a.activityDuration')
            ->where('a.date = :date')
            ->orderBy('a.activityDuration', 'DESC')
            ->setParameter(':date', $date);

        $qb = $query->getQuery();

        return $qb->getResult();
    }
}
