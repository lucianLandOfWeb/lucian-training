<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ImageRepository extends EntityRepository
{
    public function getGalleryImage($galleryId, $limit, $start)
    {
        $query = $this->createQueryBuilder('image')
            ->leftJoin('image.galleryId', 'gallery_image')
            ->where('image.galleryId = :galleryId')
            ->andWhere('image.id > :start' )
            ->setParameter('galleryId', $galleryId)
            ->setParameter('start', $start)
            ->setMaxResults($limit)
            ->getQuery()
            ->getScalarResult();

        return $query;
    }
}
