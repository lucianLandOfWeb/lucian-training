<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\DateTime;

class UserRepository extends EntityRepository
{
    public function getMostActiveUsers($date, $activityType)
    {
        $query = $this->createQueryBuilder('u')
            ->leftJoin('u.activities', 'a')
            ->where('a.date = :date')
            ->orderBy('a.number_of_likes', 'DESC')
            ->getQuery();

        return $query->getResult();
    }
}

