<?php

namespace AppBundle\Fixtures;

use AppBundle\Entity\Gallery;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class GalleryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        for ($i = 0; $i <= 20; $i++) {
            $gallery = new Gallery();
            $gallery->setName('gallery'.$i);
            $gallery->setDescription('descriere'.$i);
            $gallery->setIsActive(1);
            $gallery->setCreatedAt(new DateTime('2019-01-01'));
            $gallery->setFavouriteCounter($i);

            $manager->persist($gallery);
        }

        $manager->flush();
    }
}