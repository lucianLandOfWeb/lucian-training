<?php

namespace AppBundle\Fixtures;

use AppBundle\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i <= 20; $i++) {
            $user = new User();
            $user->setFirstName('lucian'. $i);
            $user->setLastName('stan'. $i);
            $user->setEmail('stan.lucian'. $i . '@gmail.com');
            $user->setPhoneNumber('07239123'. $i);
            $user->setPassword('bnweor'. $i);
            $user->setCreatedAt(new DateTime('now'));
            $user->setUsername('lucian.stan'. $i);
            $user->setLastLogin(new DateTime('now'));
            $user->setEnabled(1);

            $manager->persist($user);
        }

        $manager->flush();
    }
}