<?php

namespace AppBundle\Controller\Gallery;

use AppBundle\Entity\Gallery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AjaxGetGalleryController extends Controller
{
    /**
     * @Route("/galleryIndex", name = "gallery_index")
     */
    public function indexAction()
    {
        $page = 1;
        $limit = 5;
        $offset = ($page - 1) * $limit;
        $em = $this->getDoctrine()->getManager();
        $galleries = $em->getRepository(Gallery::class)->findBy([],[],$limit, $offset);

        $allGalleries = $em->getRepository(Gallery::class)->findaLL();
        $maxPages = ceil(count($allGalleries)/$limit);
        $thisPage = $page;

        return $this->render('gallery/index.html.twig', [
            'galleries' => $galleries,
            'maxPages' => $maxPages,
            'thisPage' => $thisPage,
        ]);
    }

    /**
     * @Route("/galleries", name = "view_galleries")
     * @param Request $request
     * @return JsonResponse
     */
    public function getGalleriesAction(Request $request)
    {
        $currentPage = $request->get('currentPage');
        $limit = 5;
        $em = $this->getDoctrine()->getManager();
        $galleryRepository = $em->getRepository(Gallery::class);
        $galleries = $galleryRepository->getAllGalleries($currentPage, $limit);

        return new JsonResponse($galleries);
    }
}