<?php

namespace AppBundle\Controller\Gallery;

use AppBundle\Entity\Gallery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class GetGalleryController extends Controller
{
    /**
     * @Route("/gallery/{currentPage}", name="gallery_list")
     */
    public function indexAction($currentPage)
    {
        $limit = 3;
        $em = $this->getDoctrine()->getManager();
        $galleryRepository = $em->getRepository(Gallery::class);

        $allGalleries = $galleryRepository->getMostRecentGalleries($currentPage , $limit);
        $maxPages = ceil(count($allGalleries)/$limit);
        $thisPage = $currentPage;

        $mostPopularGalleries = $galleryRepository->getMostPopularGalleries();

        return $this->render('gallery/gallery.html.twig',
            [
                'galleries' => $allGalleries,
                'mostPopularGalleries' => $mostPopularGalleries,
                'maxPages' => $maxPages,
                'thisPage' => $thisPage
            ]
        );
    }
}