<?php

namespace AppBundle\Controller\Gallery;

use AppBundle\Entity\Gallery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchGalleryController extends Controller
{
    /**
     * @Route("search", name = "search_gallery")
     */
    public function indexAction(Request $request)
    {
        $searchValue = $request->get('galleryName');

        $em = $this->getDoctrine()->getManager();
        $galleryRepository = $em->getRepository(Gallery::class);
        $galleries = $galleryRepository->getSearchedGalleries($searchValue);

        return new JsonResponse($galleries);
    }
}