<?php

namespace AppBundle\Controller\UserActivityLog;

use AppBundle\Entity\ActivityLog;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class UserActivityLogController extends Controller
{
    /**
     * @Route("/activityLog", name="activity_log")
     */
    public function indexAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $activityLogRepository = $entityManager->getRepository(ActivityLog::class);
        $date = new DateTime('2019-01-04');

        $usersWithMostLikes = $activityLogRepository->getUsersWithMostLikes($date->modify('previous day'));
        $mostActiveUsers = $activityLogRepository->getMostActiveUsers($date->format('Y-m-d'));

        return $this->render('activityLog/index.html.twig',
            [
                'usersWithMostLikes' => $usersWithMostLikes,
                'mostActiveUsers' => $mostActiveUsers
            ]);
    }
}