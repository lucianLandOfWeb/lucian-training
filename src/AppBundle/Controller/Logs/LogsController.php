<?php

namespace AppBundle\Controller\Logs;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class LogsController extends Controller
{
    /**
     * @Route("/logs", name="logs")
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $message = 'salutare logs';
        $this->get('app.log_service')->createCustomLogs($message);

        return $this->render('logs/index.html.twig');
    }
}