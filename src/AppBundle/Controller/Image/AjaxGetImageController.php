<?php

namespace AppBundle\Controller\Image;

use AppBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AjaxGetImageController extends Controller
{
    /**
     * @Route("getImage/{galleryId}", name="get_gallery_image")
     * @param $galleryId
     * @return JsonResponse
     */
    public function indexAction($galleryId)
    {
        $start = 0;
        $limit = 2;

        $em = $this->getDoctrine()->getManager();
        $imageRepository = $em->getRepository(Image::class);
        $images = $imageRepository->getGalleryImage($galleryId, $limit, $start);

        return new JsonResponse($images);
    }

    /**
     * @Route("getImage/{galleryId}/{numberOfImages}", name="load_more_images")
     * @param Request $request
     * @return JsonResponse
     */
    public function loadMoreImagesA(Request $request)
    {
        $limit = 2;
        $numberOfImages = $request->get('start');
        $galleryId = $request->get('galleryId');

        $em = $this->getDoctrine()->getManager();
        $imageRepository = $em->getRepository(Image::class);
        $images = $imageRepository->getGalleryImage($galleryId, $limit, $numberOfImages);

        return new JsonResponse($images);
    }
}