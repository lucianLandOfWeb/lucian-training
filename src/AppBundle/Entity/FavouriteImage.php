<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * FavouriteImage
 *
 * @ORM\Table(name="favourite_image")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FavouriteImageRepository")
 */
class FavouriteImage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="added_at", type="datetime")
     */
    private $addedAt;

    /**
     * @var int
     */
    private $imageId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set addedAt
     *
     * @param DateTime $addedAt
     *
     * @return FavouriteImage
     */
    public function setAddedAt($addedAt)
    {
        $this->addedAt = $addedAt;

        return $this;
    }

    /**
     * Get addedAt
     *
     * @return DateTime
     */
    public function getAddedAt()
    {
        return $this->addedAt;
    }

    /**
     * @return mixed
     */
    public function getImageId()
    {
        return $this->imageId;
    }
}

