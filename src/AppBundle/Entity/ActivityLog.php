<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ActivityLog
 *
 * @ORM\Table(name="activity_log")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ActivityLogRepository")
 */
class ActivityLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="activity_duration", type="integer")
     */
    private $activityDuration;

    /**
     * @var int
     *
     * @ORM\Column(name="number_of_likes", type="integer")
     */
    private $numberOfLikes;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return ActivityLog
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ActivityLog
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set activityDuration
     *
     * @param integer $activityDuration
     *
     * @return ActivityLog
     */
    public function setActivityDuration($activityDuration)
    {
        $this->activityDuration = $activityDuration;

        return $this;
    }

    /**
     * Get activityDuration
     *
     * @return int
     */
    public function getActivityDuration()
    {
        return $this->activityDuration;
    }

    /**
     * Set numberOfLikes
     *
     * @param integer $numberOfLikes
     *
     * @return ActivityLog
     */
    public function setNumberOfLikes($numberOfLikes)
    {
        $this->numberOfLikes = $numberOfLikes;

        return $this;
    }

    /**
     * Get numberOfLikes
     *
     * @return int
     */
    public function getNumberOfLikes()
    {
        return $this->numberOfLikes;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ActivityLog
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}

