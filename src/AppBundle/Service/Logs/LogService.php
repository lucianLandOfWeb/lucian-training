<?php

namespace AppBundle\Service\Logs;

use Psr\Log\LoggerInterface;

class LogService
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function createCustomLogs($logMessage)
    {
        $this->logger->info('aceasta e o informare');
        $this->logger->debug(sprintf('our log message is: %s ', $logMessage));
        $this->logger->warning('this is a warning', [
            'OurMessage' => $logMessage
        ]);

    }
}